import os
import logging
import datetime
from enum import Enum
from dataclasses import dataclass

from astral import geocoder, sun


class Quadrant(Enum):
    upper_left = 2
    lower_left = 1
    lower_right = 0
    upper_right = 3


@dataclass
class Reading:
    start: datetime.datetime
    cursor: datetime.datetime
    end: datetime.datetime
    quadrant: Quadrant
    quadrant_character: chr = '◢'

    def __post_init__(self):
        assert self.start < self.cursor < self.end

    def progress(self):
        return int((self.cursor - self.start) / (self.end - self.start) * 256)

    def __str__(self):
        return '{} {:02x}'.format(
            chr(ord(self.quadrant_character) + self.quadrant.value),
            self.progress()
        )


def _date_offset(date, offset):
    return datetime.date.fromordinal(date.toordinal() + offset)


def _solar_date(observer, time):
    mid = time.date()
    upper = _date_offset(mid, 1)
    lower = _date_offset(mid, -1)
    if time > sun.midnight(observer, upper):
        return upper
    if time > sun.midnight(observer, mid):
        return mid
    if time > sun.midnight(observer, lower):
        return lower
    assert False  # should not be reached, all valid cases covered


def _calculate(observer, time=None):
    if time is None:
        time = datetime.datetime.now(datetime.timezone.utc)
    date = _solar_date(observer, time)
    low = sun.midnight(observer, date)
    sun_up = sun.dawn(observer, date)
    high = sun.noon(observer, date)
    sun_down = sun.dusk(observer, date)
    next_low = sun.midnight(observer, _date_offset(date, 1))
    if time < sun_up:
        return Reading(low, time, sun_up, Quadrant.lower_left)
    if time < high:
        return Reading(sun_up, time, high, Quadrant.upper_left)
    if time < sun_down:
        return Reading(high, time, sun_down, Quadrant.upper_right)
    if time < next_low:
        return Reading(sun_down, time, next_low, Quadrant.lower_right)
    assert False  # should not be reached, all valid cases covered


def main():
    if os.getenv('SUNDIAL_DEBUG'):
        logging.basicConfig(level=logging.DEBUG)
    loc_name = os.getenv('SUNDIAL_LOCATION') or 'Los Angeles'
    observer = geocoder.lookup(loc_name, geocoder.database()).observer
    print(str(_calculate(observer)))


if __name__ == '__main__':
    main()
